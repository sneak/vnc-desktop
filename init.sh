#!/bin/bash

echo 'Updating /etc/hosts file...'
HOSTNAME=$(hostname)
echo "127.0.1.1\t$HOSTNAME" >> /etc/hosts

echo "Starting VNC server"
vncserver -kill :1 || true
vncserver -geometry 1600x1200
#vncserver -geometry 1600x1200 -randr 1920x1080,1600x1200,1440x900,1024x768 &

echo "VNC server started at $RESOLUTION! ^-^"

export DISPLAY=":1"

xset -dpms &
xset s noblank &
xset s off &
/usr/bin/startxfce4 --replace > $HOME/wm.log &

xterm &
sleep 1

tail -f $HOME/wm.log $HOME/.vnc/*.log
