#!/bin/bash

set -x


PKGS="
    alacritty
    binutils
    build-essential
    byobu
    ca-certificates
    ctags
    cups-common
    curl
    desktop-base
    elementary-xfce-icon-theme
    htop
    jq
    less
    libnotify-bin
    lsb-release
    make
    netbase
    pastebinit
    publicsuffix
    run-one
    screen
    sudo
    ttf-ubuntu-font-family
    update-notifier-common
    vim-doc
    vim-scripts
    wget
    wordlist
    xarchiver
    xdg-user-dirs
    xfdesktop4
    xfdesktop4-data
    zsh
    zsh-doc
    zstd
"

export DEBIAN_FRONTEND=noninteractive

apt install -y PKGS

install -d -m 0755 /etc/apt/keyrings
wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | tee -a /etc/apt/sources.list.d/mozilla.list > /dev/null
echo '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' | tee /etc/apt/preferences.d/mozilla
apt update
apt install -y firefox


apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
