# Use an official Ubuntu base image
FROM ubuntu:22.04

# Avoid warnings by switching to noninteractive for the build process
ENV DEBIAN_FRONTEND=noninteractive

ENV USER=root

# update system first
RUN apt update && apt upgrade -y

# Install XFCE, VNC server, dbus-x11, and xfonts-base
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    dbus-x11 \
    tightvncserver \
    vim \
    xfce4 \
    xfce4-goodies \
    xfonts-base \
    xinit \
    wget \
    curl \
    xterm

WORKDIR /tmp
COPY ./install.sh ./install.sh
RUN bash ./install.sh

# Setup VNC server
RUN mkdir /root/.vnc \
    && echo "password" | vncpasswd -f > /root/.vnc/passwd \
    && chmod 600 /root/.vnc/passwd

COPY ./xstartup /root/.vnc/xstartup

# Create an .Xauthority file
RUN touch /root/.Xauthority

RUN chmod ugoa+rx /root/.vnc/xstartup

# Set display resolution (change as needed)
ENV RESOLUTION=1920x1080

# Expose VNC port
EXPOSE 5901

# Set the working directory in the container
WORKDIR /app

# Copy a script to start the VNC server
COPY init.sh init.sh
RUN chmod +x init.sh

CMD ["/app/init.sh"]
