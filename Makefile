export DOCKER_HOST := ssh://root@las1

build:
	docker build \
		-t sneak/vnc-desktop \
		--progress plain \
		.

run:
	docker rm -f vnc-desktop
	docker run \
		-d -p 5901:5901 \
		--name vnc-desktop \
		--security-opt seccomp=unconfined \
		sneak/vnc-desktop

logs:
	docker logs -f vnc-desktop
